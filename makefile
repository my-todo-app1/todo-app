#---SYMFONY--#
DOCKER_COMPOSE = docker-compose
DOCKER_EXEC = docker exec
API_CONTAINER_NAME = todo_backend
#------------#

##
## HELP
help: ## Show this help.
	@echo "Command helper"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

# Application
info: ## Shows docker info
		@docker info

# build app (first time)
build:
		$(DOCKER_COMPOSE) up -d
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) composer init-db
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) composer config-app

# delete app
delete-apps:
		$(DOCKER_COMPOSE) down

# erase env app (delete apps && delete images front & back)
clean-apps:
		@make delete-apps
		@docker rmi $(shell docker images | grep "todo-app-front" | awk '{print $3}')
		@docker rmi $(shell docker images | grep "todo-app-api" | awk '{print $3}')

# cache clear 
clear-cache:
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) $(CONSOLE) c:c

# run messenger 
run-messenger:
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) composer exec-tasks


.PHONY: info build delete-apps clean-apps clear-cache run-messenger
