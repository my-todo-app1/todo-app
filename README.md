# Todo API

This is simple app  allows you to create todo items

## Features

- Creates users
- Log as user (Get jwt for below features)
- Refresh token (jwt)
- Get Todos (private and public): public todos is for others users
- Create todos (private or public)
- Update todos
- Change todo status 
- Delete todo

## Run Locally


```bash
  make build
```

via the Browser

```bash
  APP: http://localhost:5000 (admin account: joe.developer@test.com/0000)
  API: http://localhost:5001/api
  PMA: http://localhost:5003/
  MailDev: http://localhost:5002/#/
```

## Running Tests

Running tests, run the following command

```bash
  make run-tests
```

## Delete apps

Deleting all apps, run the following command

```bash
  make clean-apps
```

## Run messenger (bus messages)

Run messenger, run the following command.
/!\ YOU MUST RUN THIS COMMAND, beacuse some tasks are async like sending email /!\

```bash
  make run-messenger
```